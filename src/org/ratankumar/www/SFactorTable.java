package org.ratankumar.www;
//this java file will take all the entities from SFactor and make them unique , and put them in SFactorFinal .

import java.io.IOException;
import javax.servlet.http.*;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.PropertyProjection;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;

@SuppressWarnings("serial")
public class SFactorTable extends HttpServlet {


	@SuppressWarnings({ "deprecation" })
	public void doGet(HttpServletRequest req,
                      HttpServletResponse resp)
        throws IOException {
        DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
        
        Query q = new Query("SFactor");
        q.addProjection(new PropertyProjection("catname", String.class));
        q.setDistinct(true);
        PreparedQuery pq = ds.prepare(q);
        for (Entity result : pq.asIterable()) {
        		String cn = (String) result.getProperty("catname");
        		Query q2 = new Query("SFactor");
                q2.addProjection(new PropertyProjection("uno", String.class));
                q2.setDistinct(true);
                PreparedQuery pq2 = ds.prepare(q2);
                
                for (Entity result2 : pq2.asIterable()) {
                	String uno = (String) result2.getProperty("uno");
                	Query q3 = new Query("SFactor");
            		q3.addFilter("catname", Query.FilterOperator.EQUAL,cn);
            		q3.addFilter("uno", Query.FilterOperator.EQUAL,uno);
            		PreparedQuery pq3 = ds.prepare(q3);
            		int sf=0;
            		try{
            			for (Entity result3 : pq3.asIterable()) {		
            		
            			sf=sf+Integer.parseInt((String) result3.getProperty("sf"));
    				}
            		}
            		finally
            		{
            			Entity e = new Entity("SFactorFinal");
						    e.setProperty("uno",uno);
						    e.setProperty("catname",cn);
						    e.setProperty("sf",sf);
						    ds.put(e);
            		}
                }
        		
                
                
                
        		
       
        }	
    }
}