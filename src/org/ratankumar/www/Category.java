package org.ratankumar.www;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.*;

import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import java.io.*;
import java.util.Date;

@SuppressWarnings("serial")

public class Category extends HttpServlet {
	static int catNumber =0 ;
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("application/json; charset=utf-8");
		resp.setHeader("Cache-Control", "no-cache");
		PrintWriter out = resp.getWriter();
		 resp.setContentType("text/html");
		Date date = new Date();
		
		long date_unix = System.currentTimeMillis() / 1000L;
		    String title = "Reading Three Request Parameters In Category " + date + " In unix format " + date_unix;
		    com.google.appengine.api.datastore.DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		    out.println(ServletUtilities.headWithTitle(title) +
		                "<BODY>\n" +
		                "<H1 ALIGN=CENTER>" + title + "</H1>\n" +
		                "<UL>\n" +
		                "  <LI>param1: "
		                + req.getParameter("param1") + "\n" +
		                "  <LI>param1: "+
		                 req.getParameter("param2") + "\n" +
		                "</UL>\n" + 
		                "</BODY></HTML>");
		    
		   
		    Entity e = new Entity("Category");
		    e.setProperty("catname",req.getParameter("param1"));
		    e.setProperty("url",req.getParameter("param2"));
		    ds.put(e);
		  }

		  public void doPost(HttpServletRequest request,
		                     HttpServletResponse response)
		      throws ServletException, IOException {
		    doGet(request, response);
		    
		    
		    
		  }
		}

