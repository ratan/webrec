package org.ratankumar.www;

import java.io.IOException;
import javax.servlet.http.*;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.PropertyProjection;
import com.google.appengine.api.datastore.Query;


@SuppressWarnings("serial")

public class Test extends HttpServlet {
	

	Long a[] = new Long[100];
	Long b[] = new Long[100];
	Long uno[] = new Long[100];
	Long cluster1[] = new Long[100];
	Long cluster2[] = new Long[100];
	 	
	
	 public static void bubble_srt( Long a[], int n ){
   	  int i, j;
	Long t=(long) 0;
   	  for(i = 0; i < n; i++){
   	  for(j = 1; j < (n-i); j++){
   	  if(a[j-1] > a[j]){
   	  t = a[j-1];
   	  a[j-1]=a[j];
   	  a[j]=t;
   	  }
   	  }
   	  }
   	  }
	 
	 
	 void cluster(Long arr[],int n)
	 {
	 	int i=0,p=1,q=1;
		int j=n-1,k;
	 	
	 	cluster1[0]= arr[i];
	 	cluster2[0]=arr[j];
	 	for(k=1;k<n-1;k++)
	 	{
	 		if(arr[k]-arr[i]<arr[j]-arr[k])
	 		{
	 			cluster1[p]=arr[k];
	 			p++;
	 		}
	 		else if(arr[k]-arr[i]>arr[j]-arr[k])
	 		{
	 			cluster2[q]=arr[k];
	 			q++;
	 		}
	 		else if(arr[k]-arr[i]==arr[j]-arr[k])
	 		{
	 			cluster1[p]=arr[k];
	 			cluster2[q]=arr[k];
	 			q++;
	 			p++;
	 		}
	 	}}
	
	@SuppressWarnings("deprecation")
	public void doGet(HttpServletRequest req,
                      HttpServletResponse resp)
        throws IOException {
        DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
      String cn ="";
        Query qstart = new Query("SFactorFinal");

        qstart.addProjection(new PropertyProjection("catname", String.class));
        qstart.setDistinct(true);
        
        PreparedQuery pqstart = ds.prepare(qstart);
        
        for (Entity resultstart : pqstart.asIterable()) {
        	
        	 cn = (String) resultstart.getProperty("catname");
        	System.out.print("\n\ncategory = "+cn);
        
        	 int index=0;
        	 Query q = new Query("SFactorFinal");
        	 q.addFilter("catname", Query.FilterOperator.EQUAL,cn);
        	 PreparedQuery pq = ds.prepare(q);

        	 	for (Entity result : pq.asIterable()) 
        	 			{	
        	 				a[index]=(Long) result.getProperty("sf");
        	 				b[index]=a[index];
        	 				String te=(String) result.getProperty("uno");
        	 				uno[index] = Long.parseLong(te);
        	 				index++;
        	 			}
        
        	 			System.out.print("\nbefore sorting\n");
        
        	 			for(int k=0;a[k]!=null;k++)
        	 			{
        	 					System.out.print(","+a[k]);
			
        	 			}
        	 			System.out.print("\nafter sorting\n");
        
        		bubble_srt(a,index-1);
        		
                
        		for(int k=0;a[k]!=null;k++)
        		{
        			System.out.print(","+a[k]);
        			
        			
        			
        		}
        		
        		
        		cluster(a,index);
        		System.out.print("\nafter clustering");
        		
        		
        		for(int ab=0;b[ab]!=null;ab++){
        			
        			for(int c1=0;cluster1[c1]!=null;c1++)
        				if(b[ab]==cluster1[c1])
        				{	
        						cluster1[c1]=uno[ab];
        					
        					break;
        				}
        		
        			{
        				for(int c2=0;cluster2[c2]!=null;c2++){
        					if(b[ab]==cluster2[c2])
        					{	
        							cluster2[c2]=uno[ab];
        					
        						break;
        					}
        					
        				}
        			}
        			
        		}
        		
        		System.out.print("\nprinting c1 :");
        		for(int l1=0;cluster1[l1]!=null;l1++){
        			System.out.print(","+cluster1[l1]);
        			String uno1= cluster1[l1].toString();
        			Query q3 = new Query("Uploadwithcatname");
    				q3.addFilter("uno", Query.FilterOperator.EQUAL,uno1);
    				q3.addFilter("catname", Query.FilterOperator.EQUAL,cn);
    				PreparedQuery pq3 = ds.prepare(q3);
    				for (Entity result3 : pq3.asIterable()) 
    				{
    					String url = (String) result3.getProperty("url");
    					Entity e = new Entity("Cluster1");
        				e.setProperty("uno", uno1);
        				e.setProperty("catname", cn);
        				e.setProperty("url", url);
        				 ds.put(e);
    				}
    					
    				
    		
        			
        			
        			
        		}
        		System.out.print("\nprinting c2 :");
        		for(int l1=0;cluster2[l1]!=null;l1++){
        			System.out.print(","+cluster2[l1]);	
        			
        			String uno2= cluster2[l1].toString();
        			Query q4 = new Query("Uploadwithcatname");
    				q4.addFilter("uno", Query.FilterOperator.EQUAL,uno2);
    				q4.addFilter("catname", Query.FilterOperator.EQUAL,cn);
    				PreparedQuery pq4 = ds.prepare(q4);
    				for (Entity result4 : pq4.asIterable()) 
    				{
    					String url = (String) result4.getProperty("url");
    					Entity e = new Entity("Cluster2");
        				e.setProperty("uno", uno2);
        				e.setProperty("catname", cn);
        				e.setProperty("url", url);
        				 ds.put(e);
    				}
    				
        		}
        		
        		/*
        		
        		System.out.print("\ncluster 1:");
        		
        		for(int k=0;cluster1[k]!=null;k++)
        		{
        			System.out.print(cluster1[k]+"#");
        			
        			for(int ab=0;ab<index;ab++)
        				if(b[ab]==cluster1[k])
        					{
        						cluster1[k]=uno[ab];
        						break;
        					}
        			System.out.print(cluster1[k]+",");
        			
        		}
        		System.out.print("\ncluster 2:");
        		for(int k=0;cluster2[k]!=null;k++)
        		{
        			System.out.print(cluster2[k]+"#");
        			
        			for(int ab=0;ab<index;ab++)
        				if(b[ab]==cluster2[k])
        					{
        						cluster2[k]=uno[ab];
        						break;
        					}
        			System.out.print(cluster2[k]+",");
        			
        		}
        		*/
   
        }	
    }
}


