package org.ratankumar.www;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;

import java.util.Date;

@SuppressWarnings("serial")

public class Upload extends HttpServlet {
	
	@SuppressWarnings("deprecation")
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		 resp.setContentType("application/json");
		Date date = new Date();
		long date_unix = System.currentTimeMillis() / 1000L;
		com.google.appengine.api.datastore.DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		  	int len,i=0;
			String l= req.getParameter("param0");
			
		if(l!=null)
				{
					len=Integer.parseInt(l);
				
				for(i=2;i<len;i++)
				{	
					String p = "param"+i;
					if (req.getParameter(p) != null)
					{
						Entity e = new Entity("Upload");
						e.setProperty("uno",req.getParameter("param1"));
						e.setProperty("url",req.getParameter(p));
						e.setProperty("timestamp", date_unix);
						datastore.put(e);
					
				    }
				}
				}
		
		
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		int reclimit = 0;
		String title = "Website Recommendation from you on  " + date ;
		resp.getWriter().write(title);
		Query q2 = new Query("Recommendation");
		q2.addFilter("uno", Query.FilterOperator.EQUAL,req.getParameter("param1"));
		com.google.appengine.api.datastore.DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		PreparedQuery pq2 = ds.prepare(q2);
		if(pq2!=null)
		{	
			for (Entity result2 : pq2.asIterable()) 
				{
					if(reclimit==10)
					{break;}
					String rec =(String) result2.getProperty("url");
					resp.getWriter().write("\n");
					resp.getWriter().write(rec);
					reclimit++;
				}
			
		}
		
		    
		   
		  }

		  public void doPost(HttpServletRequest req,
		                     HttpServletResponse resp)
		      throws ServletException, IOException {
		    
		  }
		}

