package org.ratankumar.www;

import java.io.IOException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.servlet.ServletException;
import javax.servlet.http.*;

import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import java.io.*;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

@SuppressWarnings("serial")

public class ProjectServlet extends HttpServlet {
	static int userNumber =0 ;
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		//resp.setContentType("text/plain");
		//resp.getWriter().println("Hello, Ratan");
		resp.setContentType("application/json; charset=utf-8");
		resp.setHeader("Cache-Control", "no-cache");
		PrintWriter out = resp.getWriter();
		 resp.setContentType("text/html");
		 // getting the current date
		Date date = new Date();
		
		long date_unix = System.currentTimeMillis() / 1000L;
		    String title = "Reading Three Request Parameters on " + date + " In unix format " + date_unix;
		    com.google.appengine.api.datastore.DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		    out.println(ServletUtilities.headWithTitle(title) +
		                "<BODY>\n" +
		                "<H1 ALIGN=CENTER>" + title + "</H1>\n" +
		                "<UL>\n" +
		                "  <LI>param1: "
		                + req.getParameter("param1") + "\n" +
		                "  <LI>param2: "
		                + req.getParameter("param2") + "\n" +
		                "  <LI>param3: "
		                + req.getParameter("param3") + "\n" +
		                "  <LI>param4: "
		                + req.getParameter("param4") + "\n" +
		                "  <LI>param5: "
		                + req.getParameter("param5") + "\n" +
		                "</UL>\n" + 
		                "</BODY></HTML>");
		    
		    String UserID = "U"+userNumber++;
		    
		    String UID = "UID";
		    String pass= "mypassword";
		    
		    Entity e = new Entity("User",UID);
		    e.setProperty("UID",UserID);
		    e.setProperty("username",req.getParameter("param1"));
		    e.setProperty("FirstName",req.getParameter("param2"));
		    e.setProperty("LastName",req.getParameter("param3"));
		    e.setProperty("email",req.getParameter("param4"));
		    e.setProperty("contactno",req.getParameter("param5"));
		    //e.setProperty("CreationTime",date_unix);
		    //storing password
		    
		    
		    
		    try{
		    	 
			    KeyGenerator keygenerator = KeyGenerator.getInstance("DES");
			    SecretKey myDesKey = keygenerator.generateKey();
	 
			    Cipher desCipher;
	 
			    // Create the cipher 
			    desCipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
	 
			    // Initialize the cipher for encryption
			    desCipher.init(Cipher.ENCRYPT_MODE, myDesKey);
			    
			    //sensitive information encryption
			    byte[] text = pass.getBytes();
	 
			    //System.out.println("Text [Byte Format] : " + text);
			    //System.out.println("Text : " + new String(text));
	 
			    // Encrypt the text
			    byte[] textEncrypted = desCipher.doFinal(text);
			    
			    e.setProperty("password", textEncrypted.toString());
	 
			    //System.out.println("Text Encryted : " + textEncrypted);
	 
			    // Initialize the same cipher for decryption
			    desCipher.init(Cipher.DECRYPT_MODE, myDesKey);
	 
			    // Decrypt the text
			   // byte[] textDecrypted = desCipher.doFinal(textEncrypted);
	 
			    //System.out.println("Text Decryted : " + new String(textDecrypted));
	 
			}catch(NoSuchAlgorithmException e1){
				e1.printStackTrace();
			}catch(NoSuchPaddingException e1){
				e1.printStackTrace();
			}catch(InvalidKeyException e1){
				e1.printStackTrace();
			}catch(IllegalBlockSizeException e1){
				e1.printStackTrace();
			}catch(BadPaddingException e1){
				e1.printStackTrace();
			} 
	
		    
		    
		    
		    
		    
		    ds.put(e);
		  }

		  public void doPost(HttpServletRequest request,
		                     HttpServletResponse response)
		      throws ServletException, IOException {
		    doGet(request, response);
		    
		    
		    
		  }
		}

