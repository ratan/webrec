package org.ratankumar.www;
//this file will take the Entities from Upload , then will compare them to Category , if found then will put them in SFactor table else will put them in Annonymous table .
import java.io.IOException;
import javax.servlet.http.*;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;

@SuppressWarnings("serial")
public class QueriesServlet extends HttpServlet {


	@SuppressWarnings({ "deprecation", "finally" })
	public void doGet(HttpServletRequest req,
                      HttpServletResponse resp)
        throws IOException {
        DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
       
        
        Query q = new Query("Upload");
        PreparedQuery pq = ds.prepare(q);
        
        for (Entity result : pq.asIterable()) {
        
        		String url = (String) result.getProperty("url");
        		String uid = (String) result.getProperty("uno");
        		String catname = null ;
        		int sf ;
        		DatastoreService ds1 = DatastoreServiceFactory.getDatastoreService();
        		Query q2 = new Query("Category");
        		q2.addFilter("url", Query.FilterOperator.EQUAL,url);
        		PreparedQuery pq2 = ds1.prepare(q2);
        		
        			if(pq2!=null)
        			{
        				for (Entity result2 : pq2.asIterable()) 
        					{
        						catname = (String) result2.getProperty("catname");
        						
        						Query q3 = new Query("SFactor");
                				q3.addFilter("uno", Query.FilterOperator.EQUAL,uid);
                				q3.addFilter("catname", Query.FilterOperator.EQUAL,catname);
                				PreparedQuery pq3 = ds.prepare(q3);
                				try{
                				for (Entity result3 : pq3.asIterable()) 
                				{
                						
                						
                						sf = (int) Integer.parseInt((String) result3.getProperty("sf"));
                						sf=sf+1;
                						String temp= Integer.toString(sf);
                						result3.setProperty("sf",temp);
                						result3.setProperty("uno",uid);
                						ds.put(result3);
                				}
                				}
                				finally{
            						
            							    Entity e = new Entity("SFactor");
            							    e.setProperty("uno",uid);
            							    e.setProperty("catname",catname);
            							    e.setProperty("sf", "1");
            							    ds.put(e);
            							    break;
            						
                				}
                				
                				
        					}
        			}
        			else
        			{
        				Entity e = new Entity("Anonymous");
					    e.setProperty("uno",uid);
					    e.setProperty("url",url);
					    ds.put(e);
        			}
        		
        }	
    }
}