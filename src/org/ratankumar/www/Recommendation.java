package org.ratankumar.www;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.PropertyProjection;
import com.google.appengine.api.datastore.Query;

import java.io.*;
import java.util.Date;

@SuppressWarnings("serial")

public class Recommendation extends HttpServlet {
	
	@SuppressWarnings("deprecation")
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		resp.setContentType("application/json; charset=utf-8");
		resp.setHeader("Cache-Control", "no-cache");
		PrintWriter out = resp.getWriter();
		resp.setContentType("text/html");
		Date date = new Date();
		String title = " Web sites Recommended for you on : " + date;
		
		com.google.appengine.api.datastore.DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		//generate the recommendation
			//String uno = req.getParameter("param0");
		  	Query qstart = new Query("Upload");
	        qstart.addProjection(new PropertyProjection("uno", String.class));
	        qstart.setDistinct(true);
	        PreparedQuery pqstart = datastore.prepare(qstart);
	        for (Entity resultstart : pqstart.asIterable()) {
	        	
	        	
	       String uno = (String) resultstart.getProperty("uno"); 
			Query q = new Query("Cluster1");
			q.addFilter("uno", Query.FilterOperator.EQUAL,uno);
			PreparedQuery pq = datastore.prepare(q);
			if(pq!=null)
			{
				Query q2 = new Query("Cluster1");
				q2.addFilter("uno", Query.FilterOperator.NOT_EQUAL,uno);
				PreparedQuery pq2 = datastore.prepare(q2);
				for(Entity result:pq2.asIterable())
				{
					String url =(String) result.getProperty("url");
					Entity e = new Entity("Recommendation");
					e.setProperty("uno", uno);
					e.setProperty("url", url);
					datastore.put(e);
				}
			}
			else 
			{
				
				Query q2 = new Query("Cluster2");
				q2.addFilter("uno", Query.FilterOperator.NOT_EQUAL,uno);
				PreparedQuery pq2 = datastore.prepare(q2);
				if(pq2!=null){
				for(Entity result:pq2.asIterable())
				{
					String url =(String) result.getProperty("url");
					Entity e = new Entity("Recommendation");
					e.setProperty("uno", uno);
					e.setProperty("url", url);
					datastore.put(e);
				}
				}
			}
		

		
		
		
		
	        }
		
		
		out.println(ServletUtilities.headWithTitle(title) +
		                "<BODY>\n" +
		                "<H1 ALIGN=CENTER>" + title + "</H1>\n" +
		                "<UL>\n" +
		                " Recommendations for :</br> <LI>param1: "
		                + "\n" +
		                "</UL>\n" + 
		                "</BODY></HTML>");
		   
		  }

		  public void doPost(HttpServletRequest req,
		                     HttpServletResponse resp)
		      throws ServletException, IOException {
	
		    
		  }
		}

