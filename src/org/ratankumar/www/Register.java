package org.ratankumar.www;

import java.io.IOException;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.http.*;

import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import java.io.*;
import java.util.Date;
import java.util.Properties;

@SuppressWarnings("serial")

public class Register extends HttpServlet {
	
	  public void send(String toAddress, String subject, String msgBody)
		      throws IOException {

		    Properties props = new Properties();
		    Session session = Session.getDefaultInstance(props, null);

		    try {
		      Message msg = new MimeMessage(session);
		      String fromAddress="mail@ratankumar.org";
			msg.setFrom(new InternetAddress(fromAddress));
		      InternetAddress to = new InternetAddress(toAddress);
		      msg.addRecipient(Message.RecipientType.TO, to);
		      msg.setSubject(subject);
		      msg.setText(msgBody);
		      Transport.send(msg, new InternetAddress[] { to });

		    } catch (AddressException addressException) {
		      //log.log(Level.SEVERE, "Address Exception , mail could not be sent", addressException);
		    } catch (MessagingException messageException) {
		      //log.log(Level.SEVERE, "Messaging Exception , mail could not be sent", messageException);
		    }
		  }
	  
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		
		
		
			resp.setContentType("application/json; charset=utf-8");
			resp.setHeader("Cache-Control", "no-cache");
			PrintWriter out = resp.getWriter();
			 resp.setContentType("text/html");
			 // getting the current date
			Date date = new Date();
			
			long date_unix = System.currentTimeMillis() / 1000L;
			    String title = "Reading Three Request Parameters on " + date + " In unix format " + date_unix;
			    com.google.appengine.api.datastore.DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
			    out.println(ServletUtilities.headWithTitle(title) +
			                "<BODY>\n" +
			                "<H1 ALIGN=CENTER>" + title + "</H1>\n" +
			                "<UL>\n" +
			                "  <LI>param1: "
			                + req.getParameter("name") + "\n" +
			                "  <LI>param2: "
			                + req.getParameter("email") + "\n" +
			                "</UL>\n" +"  <LI>Congratulations you have been registered please check your email for Chrome Extension OR "+ 
			                "</BODY></HTML>"+"<a href=\"webrec.zip\">DOWNLOAD HERE</a>");
			    
			    
			    String name=req.getParameter("name");
			    String email = req.getParameter("email");
			    
			    Entity e = new Entity("Register");
			    e.setProperty("username",name);
			    e.setProperty("email",email);
			    e.setProperty("CreationTime",date_unix);
			    ds.put(e);
			    String link = "project.ratankumar.org/webrec.zip";
			    String msgBody= "Hi ,"+name + ". Congratulations for Getting WebRec -Google Chrome Extension , your personalised web based recommendations system "+link;
			    send(email,"Congratulatinos for Getting Webrec", msgBody);
			    

	}

public void doPost(HttpServletRequest req,
		                     HttpServletResponse resp)
		      throws ServletException, IOException {
		   resp.setContentType("application/json; charset=utf-8");
		   resp.setHeader("Cache-Control", "no-cache");
		   PrintWriter out = resp.getWriter();
		   resp.setContentType("text/html");
		   Date date = new Date();
			
			long date_unix = System.currentTimeMillis() / 1000L;
			    String title = "Reading Three Request Parameters on " + date + " In unix format " + date_unix;
			    com.google.appengine.api.datastore.DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
			    out.println(ServletUtilities.headWithTitle(title) +
			                "<BODY>\n" +
			                "<H1 ALIGN=CENTER>" + title + "</H1>\n" +
			                "<UL>\n" +
			                "  <LI>param1: "
			                + req.getParameter("param1") + "\n" +
			                "  <LI>param2: "
			                + req.getParameter("param2") + "\n" +
			                "</UL>\n" + "<a href=\"Category\">DOWNLOAD HERE</a>"+
			                "</BODY></HTML>");
			    
			    Entity e = new Entity("Anonymous");
			    e.setProperty("UID",req.getParameter("param1"));
			    e.setProperty("URL",req.getParameter("param2"));
			    e.setProperty("CreationTime",date_unix);
			    ds.put(e);
		    
		    
		    
		  }
		}

